<?php

    $valor = 11;

    if ($valor == 10) {
        echo "É igual a 10";
    } else {
        echo "Não é igual a 10";
    }

    $valor1 = "amaury";
    $valor2 = "fulano";

    echo "<br />";

    if ($valor1 == $valor2) {
        echo "São iguais";
    } else {
        echo "Não são iguais";
    }

    $numero = 10;
    $outroNumero = '10';

    echo "<br />";
    if ($numero == $outroNumero) {
        echo "São iguais";
    } else {
        echo "Não são iguais";
    }


    echo "<br />";
    if ($numero === $outroNumero) {
        echo "São iguais";
    } else {
        echo "Não são iguais";
    }

    echo "<br />";
    if ($numero != $outroNumero) {
        echo "Não são iguais";
    } else {
        echo "São iguais";
    }


    echo "<br />";
    if ($numero !== $outroNumero) {
        echo "Não são iguais";
    } else {
        echo "São iguais";
    }




    $myname = 'fulano';

    echo "<br /><br />";
   
    echo $myname == 'fulano' ? 'hello world' : 'olá';

    echo "<br /><br />";
    echo $myname ?? 'nada';

    if (!empty($myname)) {
        echo $myname;
    } else {
        echo 'nada';
    }

    


?>