<?php

function minhaFuncao() {
    echo 'hello world';
}

function minhaFuncaComRetorno() {
    return 'outraFuncao';
}

function retornoComInteiro() {
    return 10;
}

function comParametros($num1, $num2) {
    return $num1 + $num2;
}

function valoresPadroes($num = 10, $outroValor = 20) {
    echo $num . '<br />';
    echo $outroValor;
}

function soma(int ...$numbers) {
    $result = array_sum($numbers);

    echo $result;
}

function formatarImpressao($valor) {
    echo '<pre>';

    print_r($valor);

    echo '</pre>';
    echo '<br /><br />';
}

?>