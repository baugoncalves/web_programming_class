<?php

    ## switch case

    $nome =  'amaury';

    switch($nome) {
        case 'hello':
            echo 'O nome é hello';
            break;

        case 'amaury':
            echo 'O nome é amaury';
            break;

        default:
            echo 'nada encontrado';
    }

    echo '<br /><br />';

?>