<?php

include('funcoes.php');

$valores = array('banana', 'pera', 'uva', 'limao', 'melancia', 'maracuja');

echo 'Quantidade de count: '.count($valores).'<br />';

echo '<br /><br />';

echo '<h1>Arrays Indexados</h1>';

echo '<br /><br />';

echo $valores[0];

echo '<br /><br />';

for($i=0; $i <count($valores); $i++) {
    echo $valores[$i].'<br />';
} 

echo '<br /><br />Foreach<br /><br />';

foreach($valores as $valor) {
    echo $valor.'<br />';
}

echo '<br />';
var_dump($valores);

echo '<br /><br /><br /><br />';

print_r($valores);

echo '<br /><br /><br /><br />';

echo '<pre>';

print_r($valores);

echo '</pre>';


echo '<br /><br />';

echo '<h1>Arrays Associativos</h1>';

$idades = array('maria' => 18, 'pedro' => 20, 'joao' => 12);

echo 'Idade de joao: '.$idades['joao'];

echo '<br /><br />';

var_dump($idades);

echo '<br /><br />';

formatarImpressao($idades);

echo '<br /><br />';


echo '<h2>Loop foreach with associative arrays</h2>';

foreach ($idades as $idade) {
    echo 'Idade: '. $idade;
    echo '<br /><br />';
}

foreach($idades as $key=>$value) {
    echo "$key = $value";
    echo '<br /><br />';
}

$frutas = array("melancia", "laranja");

formatarImpressao($frutas);

array_push($frutas, 'peras', 'uvas');

formatarImpressao($frutas);;

array_pop($frutas);

formatarImpressao($frutas);

array_shift($frutas);

formatarImpressao($frutas);


?>